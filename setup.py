from setuptools import setup

setup(name='powerline-netifaces',
      version='0.1',
      description='A Powerline segment that lists details of network interfaces.',
      url='https://gitlab.com/AGausmann/powerline-netifaces',
      author='Adam Gausmann',
      author_email='adam@nonemu.ninja',
      license='GPLv3',
      packages=['powerline-netifaces'],
      install_requires=[
          'powerline-status',
          'netifaces'
      ],
      zip_safe=False)

